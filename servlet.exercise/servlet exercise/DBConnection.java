package servlet.exercise;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DBConnection
 */
@WebServlet("/DBConnection")
public class DBConnection extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 PrintWriter out=response.getWriter();
	        String firstname = request.getParameter("firstname");
	        String email = request.getParameter("email");
	        String mobilenumber = request.getParameter("mobilenumber");
	        String dob = request.getParameter("dob");
	        String gender = request.getParameter("gender");
	        String country= request.getParameter("country");
	        int mob=Integer.parseInt(mobilenumber);
	        int dob1=Integer.parseInt(dob);
	        try {
	            Class.forName("com.mysql.cj.jdbc.Driver");
	            Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mysql", "root", "root");
	            Statement st=con.createStatement();
	            st.executeUpdate("insert into registration values('"+firstname+"','"+email+"','"+mob+"','"+dob1+"','"+gender+"','"+country+"')");
	            out.println("Success");
	        } catch (Exception e) {
	        	out.println(e);

	        }
	    }
}
