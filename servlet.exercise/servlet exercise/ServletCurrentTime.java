package servlet.exercise;
import java.io.PrintWriter;
import java.util.GregorianCalendar;
import java.util.Calendar;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletCurrentTime extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 
        response.setIntHeader("Refresh", 1);
        response.setContentType("text/html");
        Calendar calendar = new GregorianCalendar();
       
        int hour = calendar.get(Calendar.HOUR);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        String CT = hour+":"+ minute +":"+ second;
        PrintWriter out = response.getWriter();
        out.println("<h1 align='center'>Auto Refresh Page</h1><hr>");
        out.println("<h3 align='center'>Current time: "+CT+"</h3>");
   }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		doGet(request, response);
	}

}
