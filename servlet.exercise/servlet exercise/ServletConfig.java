package servlet.exercise;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
/**
 * Servlet implementation class ServletConfig
 */
@WebServlet("/ServletConfig")
public class ServletConfig extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		String n1=getInitParameter("n1");
		String n2=getInitParameter("n2");
		out.println("Number1 value is:"+n1+"<br>");
		out.println("Number2 value is:"+n2+"<br>");
		int sum=Integer.parseInt(n1)+Integer.parseInt(n2);
		out.println("Result is:"+sum);
		}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
