package servlet.exercise;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class EligibleForVoteOrNot
 */
@WebServlet("/EligibleForVoteOrNot")
public class EligibleForVoteOrNot extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 PrintWriter out=response.getWriter();
			response.setContentType("text/html");
			 String name=request.getParameter("name");
			 String age=request.getParameter("age");
			 int age1 =Integer.parseInt(age);
			 if(age1<18)
			 {
				 out.println(name+" is not eligible for vote");	 
			 }
			 else
			 {
				 out.println(name+" is eligible for vote");	 
			 }			
	}

}
