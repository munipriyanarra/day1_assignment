package Day6exercise;

public class ConstructorThread1 extends Thread{
	ConstructorThread1(){
		Thread thread=new Thread();
		thread.start();	
	}

	public static void main(String[] args) {
		ConstructorThread1 t1=new ConstructorThread1();
		ConstructorThread1 t2=new ConstructorThread1();
		ConstructorThread1 t3=new ConstructorThread1();
	}
	
	public void run() {
		for(int i=0;i<10;i++) {
			System.out.println("Running child Thread in loop :"+i);
		}

	}

}
