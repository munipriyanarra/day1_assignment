package Day6exercise;

public class ConstructorThread extends Thread{
	ConstructorThread(){
		Thread thread=new Thread();
		thread.start();	
	}

	public static void main(String[] args) {
		ConstructorThread t1=new ConstructorThread();
		ConstructorThread t2=new ConstructorThread();
		ConstructorThread t3=new ConstructorThread();
	}
	
	public void run() {
		for(int i=0;i<10;i++) {
			try {
				Thread.sleep(2000);
			}
			catch(InterruptedException e) {
				
				e.printStackTrace();
				
			}
			System.out.println("Running child Thread in loop :"+i);
		}
	}

}
