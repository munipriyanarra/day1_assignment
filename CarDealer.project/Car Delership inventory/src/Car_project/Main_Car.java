package Car_project;
import java.util.Scanner;


public class Main_Car {

	public static void main(String[] args) throws Exception {
		CarDealer car=new CarDealer();
		Scanner sc=new Scanner(System.in);
		String choice;
		System.out.println("Welcome to Mullet joe's Gently used Auto!.....");
		do {
		System.out.println("Enter command from following list");
		System.out.println("1.Add");
		System.out.println("2.List");
		System.out.println("3.Update");
		System.out.println("4.Delete");
		System.out.println("5.Quit");
		System.out.println("Enter the command:");
	    choice =sc.next();
		switch(choice)
		{
		case "Add":
			car.addCar();
		    break;
		case "List":
			car.listCar();
			break;
			
		case "Update":
			car.updateCar();
			break;
			 
		case "Delete":
			car.deleteCar();
			break;
			
		case "Quit":
			System.out.println("GoodBye....");
			break;
			
		default:
			System.out.println("Sorry but" + " " +choice +" " + "is not a valid command.please try again.");
			break;	
			
		}

		}while(!(choice.equalsIgnoreCase("quit"))); 
		sc.close();			
	}
}
