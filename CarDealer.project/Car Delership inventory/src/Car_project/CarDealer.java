package Car_project;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class CarDealer {
	Connection con=null;
	Statement st=null;
	PreparedStatement ps=null;
	Scanner sc=new Scanner(System.in);
	
	public void addCar()throws Exception {
		System.out.println("Make");
		String make = sc.next();
		System.out.println("Model");
		String model = sc.next();
		System.out.println("Year");
		int year =sc.nextInt();
		System.out.println("Sales price");
		float price = sc.nextFloat();
		try {
			con=DBConnection.getConnection();
		    ps=con.prepareStatement("insert into car values(?,?,?,?)");
			ps.setString(1,make);
			ps.setString(2,model);
			ps.setInt(3,year);
			ps.setFloat(4,price);
		    int noOfRows=ps.executeUpdate();
		    System.out.println(noOfRows + " car inserted successfully....");
		} catch(Exception e)
		{
			System.out.println(e);
			
		}
	}
	public void listCar()throws Exception{
		float total_price=0.0f;
		try {
			con=DBConnection.getConnection();
			ps=con.prepareStatement("select * from car");
			ResultSet rs=ps.executeQuery();
		    System.out.println("Make" + "\t" + "Model" + "\t" + "year" + "\t" + "SalesPrice");
			while(rs.next()) {
				System.out.println(rs.getString(1) + "\t" + rs.getString(2) + "\t" + rs.getInt(3) + "\t" + rs.getFloat(4) + "\t");
			    total_price = total_price+rs.getFloat(4);
			}
		System.out.println("Total inventory: "+total_price);
		} catch(Exception e)
		{
			System.out.println(e);
			
		}
		
	}
	public void updateCar()throws Exception{
		System.out.println("Enter Model to be Updated:");
	    String model=sc.next();
		System.out.println("Enter Make");
		String make=sc.next();
		try {
			con=DBConnection.getConnection();
			ps=con.prepareStatement("update car set model = ? where make = ?");
			ps.setString(1,model);
			ps.setString(2,make);
			int rows=ps.executeUpdate();
			System.out.println(rows+ " car updated successfully...");
		}catch(Exception e){
			System.out.println(e);
		}
	}
	public void deleteCar()throws Exception{
		 System.out.println("Enter Model to be deleted");
		 String model=sc.next();
		    try {
		    	con=DBConnection.getConnection();
				ps=con.prepareStatement("delete from car where model = ?" );
				ps.setString(1,model);
				int row =ps.executeUpdate();
				System.out.println(row+ " car deleted successfully...."); 	
		    }catch(Exception e){
				System.out.println(e);
				
			}
		}

}
