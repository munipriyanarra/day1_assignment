package Day5exercise;
import java.util.Scanner;
import java.util.InputMismatchException ;

/*You will be given two integers and as input
   exception will occur and you have to report it. .*/

public class HandleException {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int num1=in.nextInt();
		int num2=in.nextInt();
		try {
			int output=num1/num2;
			System.out.println(output);
		}catch(ArithmeticException e) {
			System.out.println(e);
		}catch(InputMismatchException e) {
			System.out.println(e);
		}
		

	}

}
