package Day5exercise;
import java.io.FileReader;
import java.io.FileNotFoundException;

/* Handling a checked exception by opening a file
Write a code opens a text file and writes its content to the standard output. 
What happens if the file doesn�t exist?
*/

public class HandleCheckException {

	public static void main(String[] args) {
		FileReader file;
		 try {
	         file=new FileReader("file.doc");
	         System.out.println(file);
	         
	      } catch (FileNotFoundException e) { 
	         e.printStackTrace();  
	     
	      }

	}
	
}


