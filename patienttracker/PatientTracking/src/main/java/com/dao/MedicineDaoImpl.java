package com.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Medicine;


	@Repository
	public class MedicineDaoImpl implements MedicineDaoIntf{

	    @Autowired
	    SessionFactory sessionFactory;
	    public void saveMedicine(Medicine medicine) {
	        sessionFactory.openSession().save(medicine);	        
	    }

	    public List<Medicine> medicineDetails() {
	        List<Medicine> li=(List<Medicine>) sessionFactory.openSession().createQuery("from Medicine").list();
	        return li;
	    }
	    public int deleteMedicineById(int medicineid) {
	        int result=0;
	        Medicine medicine=(Medicine) sessionFactory.openSession().load(Medicine.class, medicineid);   
	        if(medicine!=null) {
	            Query query=(Query) sessionFactory.openSession().createQuery("delete from Medicine m where m.medicineid=:medicineid");
	            query.setParameter("medicineid",medicineid);
	             result=query.executeUpdate();   
	        }
	        return result;
	    }
	    @Override
		public List<Medicine> viewMedicineById(int medicineid) {
			Query query= (Query) sessionFactory.openSession().createQuery("from Medicine m where m.medicineid=?");
	        query.setParameter(0,medicineid);
	        List<Medicine> li=query.list();
	            return li;
		}
	    public Medicine getMedicineById(int medicineid) {
			SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from medicine_tbl where medicineid=:medicineid").addEntity(Medicine.class);
			query.setParameter("medicineid",medicineid);
			Medicine medicine= (Medicine) query.uniqueResult();
			return medicine;
		}

		
		public void updateMedicine(Medicine medicine) {
			SQLQuery query=sessionFactory.openSession().createSQLQuery("update medicine_tbl set medicinename=:name,medicineusage=:usage,medicinecost=:cost where medicineid=:medicineid").addEntity(Medicine.class);
			query.setParameter("name",medicine.getMedicinename());
			query.setParameter("usage",medicine.getMedicineusage());
			query.setParameter("cost",medicine.getMedicinecost());
			query.setParameter("medicineid",medicine.getMedicineid());
			query.executeUpdate();		
		}

}
