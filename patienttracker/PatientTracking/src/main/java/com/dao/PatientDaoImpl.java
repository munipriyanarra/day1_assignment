package com.dao;

import java.util.List;


import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.model.Medicine;
import com.model.Patient;
@Repository
	public class PatientDaoImpl implements PatientDaoIntf{

	
	    @Autowired
	    SessionFactory sessionFactory;
	    public void savePatient(Patient patient) {
	        sessionFactory.openSession().save(patient);
	        
	    }
		
	    public List<Patient> patientDetails() {
	        List<Patient> li=(List<Patient>) sessionFactory.openSession().createQuery("from Patient").list();
	        return li;
	    }
	    
	    public int deletePatientById(int patientid) {
	        int result=0;
	        Patient patient=(Patient) sessionFactory.openSession().load(Patient.class, patientid);   
	        if(patient!=null) {
	            Query query=(Query) sessionFactory.openSession().createQuery("delete from Patient p where p.patientid=:patientid");
	            query.setParameter("patientid",patientid);
	             result=query.executeUpdate();   
	        }
	        return result;
	    }
	   		@Override
		public List<Patient> viewPatientById(int patientid) {
			Query query= (Query) sessionFactory.openSession().createQuery("from Patient p where p.patientid=?");
	        query.setParameter(0,patientid);
	        List<Patient> li=query.list();
	            return li;
		}
	   		public Patient getPatientById(int patientid) {
	   			SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from Patient_details where patientid=:patientid").addEntity(Patient.class);
	   			query.setParameter("patientid",patientid);
	   			Patient patient= (Patient) query.uniqueResult();
	   			return patient;
	   		}

	   		
	   		public void updatePatient(Patient patient) {
	   			SQLQuery query=sessionFactory.openSession().createSQLQuery("update Patient_details set disease=:disease,firstname=:name,lastname=:lname,age=:age,gender=:gender,mobileno=:number where patientid=:patientid").addEntity(Patient.class);
	   			query.setParameter("disease",patient.getDisease());
	   			query.setParameter("name",patient.getFirstname());
	   			query.setParameter("lname",patient.getLastname());
	   			query.setParameter("age", patient.getAge());
	   			query.setParameter("gender", patient.getGender());
	   			//query.setParameter("address",patient.getAddress());
	   			query.setParameter("number",patient.getMobileno());
	   			query.setParameter("patientid",patient.getPatientid());
	   			query.executeUpdate();		
	   		}
	   		public List<Patient> getPatientDetails(int id) {
				SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from Patient_details where medicineid=:id").addEntity(Patient.class);
	   			query.setParameter("id",id);
	   			List<Patient> li=query.list();
				return li;
			}

			
			public List<Patient> getBillDetails(int id) {
				SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from Patient_details p where p.patientid=:id").addEntity(Patient.class);
				
				query.setParameter("id", id);
				List<Patient> lp=query.list();
				/*
				 * Patient pid=lp.get(0);
				 * 
				 * //List<Integer> pid= (List<Integer>) sessionFactory.openSession().
				 * createQuery("select p.medicineid from Patient p where p.patientid=:id");
				 * //List pid=query1.list(); SQLQuery squery=sessionFactory.openSession().
				 * createSQLQuery("select * from medicine_info m where m.medicineid=:mid").
				 * addEntity(Medicine.class); squery.setParameter("mid", pid);
				 * List<Medicine>lm=squery.list(); List<Object> li = new ArrayList<>();
				 * li.add(lp); //li.add(lm);
				 */				return lp;
			}

			@Override
			public List<Medicine> getMedicineDetails(int id) {
SQLQuery query=sessionFactory.openSession().createSQLQuery("select * from medicine_tbl p where p.prescriptionid=:id").addEntity(Medicine.class);
				
				query.setParameter("id", id);
				List<Medicine> lp=query.list();
				return lp;
			}
			}

