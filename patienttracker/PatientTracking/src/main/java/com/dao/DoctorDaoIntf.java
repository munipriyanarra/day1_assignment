package com.dao;

import java.util.List;

import com.model.Clerk;
import com.model.Doctor;

public interface DoctorDaoIntf {

	

	List<Doctor> doctorDetails();

	void saveDoctor(Doctor doctor);

	
	void saveEditDoctor(Doctor doctor);

	int deleteDoctorById(int id);

	List<Doctor> viewDoctorById(int doctorid);

	Doctor getDoctorById(int id);

	

	void updateDoctor(Doctor doctor);

}
