package com.service;

import com.model.AdminRegister;

public interface AdminRegisterServiceIntf {

	void saveAdminRegister(AdminRegister register);

	boolean checkAdmin(int adminid, String password);

	void forgotPassword(int adminId,String password);
	
}
