package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.MedicineDaoIntf;

import com.model.Medicine;
import com.model.Patient;



@Service
@Transactional
public class MedicineServiceImpl   implements MedicineServiceIntf{
@Autowired
    MedicineDaoIntf dao;
    

	@Override
	public void saveMedicine(Medicine medicine) {
	
		 dao.saveMedicine(medicine);
	}

	@Override
    public List<Medicine> medicineDetails() {
        List<Medicine> li=dao.medicineDetails();
        return li;
    }
	@Override
	public int deleteMedicineById(int medicineid) {
		return dao.deleteMedicineById(medicineid);
	}
	
	@Override
	public List<Medicine> viewMedicineById(int medicineid) {
		List<Medicine> medicine=dao.viewMedicineById(medicineid);
        return medicine;
		
	}
	public Medicine getMedicineById(int medicineid) {
		return dao.getMedicineById(medicineid);
}
	
public void updateMedicine(Medicine medicine) {
dao.updateMedicine(medicine);
}
}



