package com.service;

import java.util.List;

import com.model.Medicine;


public interface MedicineServiceIntf {
	void saveMedicine(Medicine medicine);
	public List<Medicine> medicineDetails();
	int deleteMedicineById(int medicineid);
	public List<Medicine> viewMedicineById(int medicineid);
	Medicine getMedicineById(int medicineid);
	void updateMedicine(Medicine medicine);
}
