package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.dao.PrescriptionDaoIntf;
import com.model.Medicine;
import com.model.Prescription;


	@Service
	@Transactional
	public class PrescriptionServiceImpl implements PrescriptionServiceIntf{
	@Autowired
	    PrescriptionDaoIntf dao;
	
	@Override
	    public void savePrescription(Prescription prescription) {
	        
	        dao.savePrescription(prescription);
	    }
	    @Override
	    public List<Prescription> prescriptionDetails() {
	        List<Prescription> li=dao.prescriptionDetails();
	        return li;
	    }
		@Override
		public int deletePrescriptionById(int presid) {
			return dao.deletePrescriptionById(presid);
		}
		@Override
		public List<Prescription> viewPrescriptionById(int presid) {
			List<Prescription> prescription=dao.viewPrescriptionById(presid);
	        return prescription;
			
		}
		public Prescription getPrescriptionById(int presid) {
			return dao.getPrescriptionById(presid);
		}


		public void updatePrescription(Prescription prescription) {
		dao.updatePrescription(prescription);
		}
		public List<Medicine> medicineDetails(int id) {
			return dao.medicineDetails(id);
}
}
