package com.service;

import java.util.List;

import com.model.Clerk;
import com.model.Doctor;

public interface DoctorServiceIntf {
	void saveDoctor(Doctor doctor);

	List<Doctor> doctorDetails();

	

	void saveEditDoctor(Doctor doctor);

	int deleteDoctorById(int id);

	List<Doctor> viewDoctorById(int doctorid);

	Doctor getDoctorById(int id);

	void updateDoctor(Doctor doctor);

	

}
