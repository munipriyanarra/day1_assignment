package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.dao.PatientDaoIntf;
import com.model.Medicine;
import com.model.Patient;
	@Service
	@Transactional
	public class PatientServiceImpl  implements PatientServiceIntf{
	@Autowired
	    PatientDaoIntf dao;
	    
	    public void savePatient(Patient patient) {
	        
	        dao.savePatient(patient);
	    }

	    @Override
	    public List<Patient> patientDetails() {
	        List<Patient> li=dao.patientDetails();
	        return li;
	    }
	  		@Override
		public int deletePatientById(int patientid) {
			return dao.deletePatientById(patientid);
		}

		@Override
		public List<Patient> viewPatientById(int patientid) {
			List<Patient> patient=dao.viewPatientById(patientid);
	        return patient;
	        
			}
		public Patient getPatientById(int patientid) {
			return dao.getPatientById(patientid);
}


public void updatePatient(Patient patient) {
	dao.updatePatient(patient);
	
}
public List<Patient> getPatientDetails(int id) {
	
	return dao.getPatientDetails(id);
}

@Override
public List<Patient> getBillDetails(int id) {
	
	return dao.getBillDetails(id);
}

@Override
public List<Medicine> getMedicineDetails(int id) {
	
	return dao.getMedicineDetails(id);
}

}







