package com.service;

import java.util.List;

import com.model.Clerk;

public interface ClerkServiceIntf {

	void saveClerk(Clerk clerk);

	List<Clerk> clerkDetails();

	Clerk getClerkById(int id);

	void updateClerk(Clerk clerk);

	int deleteClerkById(int id);

	List<Clerk> viewClerkById(int clerkid);

}
