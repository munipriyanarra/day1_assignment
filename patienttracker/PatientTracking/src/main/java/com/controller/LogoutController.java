package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LogoutController {
	@RequestMapping("/logout")
	public ModelAndView logout(Model model) {
		model.addAttribute("log", "logout successfully");
		return new ModelAndView("redirect:/AdminLogin");
		
	}

}
