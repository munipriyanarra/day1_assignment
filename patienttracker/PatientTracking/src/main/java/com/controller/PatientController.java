package com.controller;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.model.Patient;
import com.service.PatientServiceIntf;


	@Controller
	public class PatientController {
	    @Autowired
	    PatientServiceIntf service;
	@RequestMapping("/patient")
	public ModelAndView Patient(@ModelAttribute("patient") Patient patient) {
	    
	    return new ModelAndView("patient");
	    
	}
	@RequestMapping("/savePatient")
	public ModelAndView savePatient(@Validated @ModelAttribute("patient") Patient patient, BindingResult result) {
	   if(result.hasErrors()) {
	        return new ModelAndView("patient");
	    }
	    else {
	        service.savePatient(patient);
	   //return new ModelAndView("redirect:/patientdetails");
	        //return new ModelAndView("success");
	        return new ModelAndView("redirect:/patientdetails");
	    }
	    }
	
	@RequestMapping("/patientdetails")
	public ModelAndView patientDetails() {
		List<Patient> li=service.patientDetails();
		 ModelAndView model = new ModelAndView("patientdetails");
	     model.addObject("patientdetails", li);
	     return model;
	}
	@RequestMapping("/deletePatient/{patientid}")
	public ModelAndView deletePatient(@PathVariable("patientid") int patientid,Model model) {
	int result=service.deletePatientById(patientid);
	    model.addAttribute("result","deleted successfully");
	    return new ModelAndView("redirect:/patientdetails");
	}
	@RequestMapping("/searchPatientById")
    public ModelAndView searchPatientById() {
		return new ModelAndView("searchpatientid");
	}
	 @RequestMapping("/searchPatientId")
	    public ModelAndView viewPatientById(@RequestParam("patientid") int patientid) {
		 List<Patient> patient=service.viewPatientById(patientid);
	     ModelAndView model = new ModelAndView("viewpatientid");
	     model.addObject("view", patient);
	     return model;
	 }
	 @RequestMapping(value="/editPatient/{patientid}")
		public ModelAndView editPatient(@PathVariable int patientid,Model model) {
			Patient patient=service.getPatientById(patientid);
			model.addAttribute("command", patient);
			return new ModelAndView("patienteditform");
		}
		@RequestMapping("/editSavePatient")
		public ModelAndView editPatientSave(Patient patient) {
			service.updatePatient(patient);
			return new ModelAndView("redirect:/patientdetails");
         }
}