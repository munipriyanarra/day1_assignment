package com.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="DOCTOR")
@NoArgsConstructor
@AllArgsConstructor


public class Doctor {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="Doctor_id")
	
	private int id;
	@NotEmpty(message="first name should not be empty")
	@Column(name="Doctor_FirstName")
	private String firstname;
	@Column(name="Doctor_LastName")
	private String lastname;
	@Min(value=23,message="age should not be less than 23")
	@Max(value=60,message="age should not be less than 60")
	@Column(name="Doctor_Age")
	private int age;
	@Column(name="Doctor_Gender")
	private String gender;
	@Column(name="Doctor_ContactNumber")
	@Size(min=10,max=10,message="phone number should be 10 digits only")
	private String contactnumber;
	@Column(name="Doctor_Domain")
	@NotEmpty(message="please enter domain")
	private String domain;
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	

}
