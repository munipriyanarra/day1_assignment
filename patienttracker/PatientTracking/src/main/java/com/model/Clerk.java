package com.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@NoArgsConstructor
@AllArgsConstructor

@Entity
@Table(name="CLERK")
public class Clerk {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int clerkid;
	@NotEmpty(message="First name should not be empty")
	private String firstname;
	private String lastname;
	@Min(value=23,message="age should not be less than 23")
	@Max(value=60,message="age should not be less than 60")
	private int age;
	private String gender;
	@Size(min=10,max=10,message="phone number should be 10 digits only")
	private String contactnumber;
	
	public int getClerkid() {
		return clerkid;
	}
	public void setClerkid(int clerkid) {
		this.clerkid = clerkid;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getContactnumber() {
		return contactnumber;
	}
	public void setContactnumber(String contactnumber) {
		this.contactnumber = contactnumber;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

}
