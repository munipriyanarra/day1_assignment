<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
     
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Tracker</title>
<style type="text/css">
.table
{border-collapse:collapse;
}
th{
background-color:blue;
}
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  
</head>
<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">View Doctor</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="homepage">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewClerkDetails">View Clerk Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="addClerk">addClerk</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewDoctorDetails">viewDoctorDetails</a>
      </li> 
<li class="nav-item">
        <a class="nav-link" href="addDoctor">addDoctor</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">view Medicine Details</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add prescription</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">view Prescription Details</a>
      </li>  
       <li class="nav-item">
        <a class="nav-link" href="createBill">Create Bill</a>
      </li>  
       <br><br> 
       
<li class="nav-item">
        <a class="nav-link" href="logout">Log Out</a>
      </li>    
    </ul>
  </div>  
</nav>

<br><br><br>
<table border="1" class="table">
<tr><th>Id</th><th>FirstName</th><th>LastName</th><th>Age</th><th>Contact Number</th><th>Gender</th><th>Domain</th><th>Edit</th><th>Delete</th></tr>
<c:forEach var="doctor" items="${doctordetails}">  
   <tr>
<td>${doctor.id}</td>
<td>${doctor.firstname}</td>
<td>${doctor.lastname}</td>
<td>${doctor.age}</td>
<td>${doctor.contactnumber}</td>
<td>${doctor.gender}</td>
<td>${doctor.domain}</td>
 <%-- <td><a href="EditDoctor/2">Edit</a></td>
<td><a href="DeleteDoctor/id=<c:out value="${doctor.id}"/>">Delete</a></td>  --%>
<%-- <td><a href="EditDoctor/${doctor.id}"/>Edit</a></td>
<td><a href="DeleteDoctor/${doctor.id}"/>Delete</a></td>  --%> 
<td><a href="<c:url value='/editDoctor/${doctor.id}' />" >Edit</a></td>
			<td><a href="<c:url value='/deleteDoctor/${doctor.id}' />" >Delete</a></td>
</tr> 
   </c:forEach>
</table> 
</body>
</html>