<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Tracker</title>
<style type="text/css">
.view{
border-collapse:collapse;
background-color:cyan;
}
</style>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">EditClerk</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
     <li class="nav-item">
        <a class="nav-link" href="homepage">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewClerkDetails">View Clerk Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="addClerk">addClerk</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewDoctorDetails">viewDoctorDetails</a>
      </li> 
<li class="nav-item">
        <a class="nav-link" href="addDoctor">addDoctor</a>
      </li> 
       <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">view Medicine Details</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add prescription</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">view Prescription Details</a>
      </li> 
       <li class="nav-item">
        <a class="nav-link" href="createBill">Create Bill</a>
      </li>
       <br><br> 
        
<li class="nav-item">
        <a class="nav-link" href="logout">Log Out</a>
      </li>      
    </ul>
  </div>  
</nav>
<div align="center">
<h3 style="color:tomato">Edit Doctor</h3>
<table border="1" class="view">
<form:form action="/PatientTracking/editSaveDoctor" method="post">
<tr><td><form:hidden path="id" /></td></tr>
<tr><th><form:label path="firstname">FirstName</form:label></th>
<td><form:input path="firstname"/></td></tr>
<tr><th><form:label path="lastname">LastName</form:label></th>
<td><form:input path="lastname" /></td></tr>
<tr><th><form:label path="age">Age</form:label></th>
<td><input type="text" name="age" /></td></tr>
<tr><th><form:label path="gender">Gender</form:label></th>
<td><form:radiobutton path="gender" value="Male"/>Male
<form:radiobutton path="gender" value="Female"/>Female </td></tr>
<tr><th><form:label path="contactnumber">ContactNumber</form:label></th>
<td><form:input path="contactnumber" /></td></tr>
<tr><th><form:label path="domain">Domain</form:label></th>
<td><form:input path="domain" /></td></tr>
<tr><th></th><td>
<input type="submit" value="Save Doctor" class="btn btn-primary">
<input type="reset" value="Cancel" class="btn btn-danger"></td></tr>
</form:form>
</table>
</div>

</body>
</html>