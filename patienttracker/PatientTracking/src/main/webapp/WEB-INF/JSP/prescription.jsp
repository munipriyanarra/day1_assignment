<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Patient Tracker</title>
<style type="text/css">
.view{�����������
border-collapse:collapse;
background-color:#ffffff00;
}
.error{
color:red;
}
������������.bg-img{
background-image:url("E:/Pics/Presc.jpg");
}
</style>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">Add Prescription</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>


 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
     <li class="nav-item">
        <a class="nav-link" href="homepage">Home</a>
      </li>
       <li class="nav-item">
        <a class="nav-link" href="viewClerkDetails">View Clerk Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="addClerk">addClerk</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewDoctorDetails">viewDoctorDetails</a>
      </li> 
<li class="nav-item">
        <a class="nav-link" href="addDoctor">addDoctor</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">view Medicine Details</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add prescription</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">view Prescription Details</a>
      </li>  
       <li class="nav-item">
        <a class="nav-link" href="createBill">Create Bill</a>
      </li> 
       <br><br> 
        
<li class="nav-item">
        <a class="nav-link" href="logout">Log Out</a>
      </li>      
    </ul>
  </div>  
</nav>
<div align="right"><a href="searchPrescriptionById">searchPrescriptionById</a></div>
<div align="center">
<h3 style="color:tomato">Add Prescription</h3>
<div class="bg-img">
<table border="1" class="view">
<form:form action="savePrescription" modelAttribute="prescription">
<tr><th>Name</th><td><form:input path="name"/><form:errors cssClass="error" path="name"/></td></tr>
<tr><th>Age</th><td><form:input path="age"/></td></tr>
<tr><th>Weight</th><td><form:input path="weight"/></td></tr>
<tr><th>Gender</th><td>
Male:<form:radiobutton path="gender" value="Male"/>
Female:<form:radiobutton path="gender" value="Female"/><form:errors cssClass="error" path="gender"/></td></tr>
<tr><th>MedicineName</th><td><form:input path="medicinename"/><form:errors cssClass="error" path="medicinename"/></td></tr>
<tr><th>Take</th><td><form:input path="take"/><form:errors cssClass="error" path="take"/></td></tr>
<tr><th>Drug form</th><td><form:input path="drugform"/><form:errors cssClass="error" path="drugform"/></td></tr>
<tr><th>Route</th><td><form:input path="route"/><form:errors cssClass="error" path="route"/></td></tr>
<tr><th>Duration</th><td><form:input path="duration"/><form:errors cssClass="error" path="duration"/></td></tr>
<tr><th></th><td><input type="submit" value="Add" class="btn btn-primary">
<input type="reset" value="Cancel" class="btn btn-danger"></td></tr>

</form:form>
</table>
</div>
</div>
</body>
</html>