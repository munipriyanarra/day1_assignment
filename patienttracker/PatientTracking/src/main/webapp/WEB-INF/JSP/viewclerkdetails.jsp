<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Tracker</title>
<style type="text/css">
.view
{border-collapse:collapse;
}
th{
background-color:blue;
}
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
</style>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">ViewClerk</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
    <li class="nav-item">
        <a class="nav-link" href="homepage">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewClerkDetails">View Clerk Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="addClerk">addClerk</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewDoctorDetails">viewDoctorDetails</a>
      </li> 
<li class="nav-item">
        <a class="nav-link" href="addDoctor">addDoctor</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">view Medicine Details</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add prescription</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">view Prescription Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="createBill">Create Bill</a>
      </li>    
       <br><br> 
       </li> 
<li class="nav-item">
        <a class="nav-link" href="logout">Log Out</a>
      </li>     
    </ul>
  </div>  
</nav>

<br><br><br>
<table border="1" class="table" class="view">
<tr><th>Id</th><th>FirstName</th><th>LastName</th><th>Age</th><th>Contact Number</th><th>Gender</th>
<th>Edit</th><th>Delete</th>
</tr>
<c:forEach var="clerk" items="${clerkdetails}">  
   <tr>
<td>${clerk.clerkid}</td>
<td>${clerk.firstname}</td>
<td>${clerk.lastname}</td>
<td>${clerk.age}</td>
<td>${clerk.contactnumber}</td>
<td>${clerk.gender}</td>
<%-- <td><a href="EditClerk/id=<c:out value='${clerk.id}'/>">Edit</a></td>
<td><a href="/DeleteClerk/id=<c:out value='${clerk.id}'/>">Delete</a></td>  --%>
 <%-- <td><a href="EditClerk/${clerk.clerid}">Edit</a></td>
<td><a href="DeleteClerk/${clerk.clerid}">Delete</a></td>  --%>
<td><a href="<c:url value='/editClerk/${clerk.clerkid}' />" >Edit</a></td>
			<td><a href="<c:url value='/deleteClerk/${clerk.clerkid}' />" >Delete</a></td>

</tr> 
   </c:forEach>
</table> 
</body>
</html>