<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Patient Tracker</title>
<style type="text/css">
.view{
border-collapse:collapse;
background-color:cyan;
}
th{
color:tomato;
}
</style>
</head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

<body>
<nav class="navbar navbar-expand-md bg-dark navbar-dark">
  <a class="navbar-brand" href="#">create bill Details</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
    <span class="navbar-toggler-icon"></span>
  </button>

 

 <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
     <li class="nav-item">
        <a class="nav-link" href="homepage">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewClerkDetails">View Clerk Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="addClerk">addClerk</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewDoctorDetails">viewDoctorDetails</a>
      </li> 
<li class="nav-item">
        <a class="nav-link" href="addDoctor">addDoctor</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="patientdetails">View patient Details</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="patient">Add patient</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="medicinedetails">view Medicine Details</a>
      </li> 
     <li class="nav-item">
        <a class="nav-link" href="medicine">Add Medicine</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="prescription">Add prescription</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="prescriptiondetails">view Prescription Details</a>
      </li>  
      <li class="nav-item">
        <a class="nav-link" href="createBill">Create Bill</a>
      </li>
       <br><br> 
        
<li class="nav-item">
        <a class="nav-link" href="logout">Log Out</a>
      </li>      
    </ul>
  </div>  
</nav>
<br><br><br>
<div align="center">
<table border="1" class="view">
<tr><th>PatientId</th><th>Disease</th><th>FirstName</th><th>LastName</th><th>age</th>
<th>gender</th><th>MobileNo</th><th>Medicine Id</th><th>Bill</th>
            
<c:forEach var="patient" items="${patientdetails}">
 <tr>
 
<td>${patient.patientid}</td>
<td>${patient.disease}</td>
<td>${patient.firstname}</td>
<td>${patient.lastname}</td>
<td>${patient.age}</td>
<td>${patient.gender}</td>

<td>${patient.mobileno}</td> 
<td>${patient.medicineid}</td>
<%-- <td><a href="<c:url value='/edit/${patient.patientid}' />" >Edit</a></td> --%>
<%-- <td><a href="<c:url value='/deletePatient/${patient.patientid}' />" >Delete</a></td> --%>
<td><a href="<c:url value='/generateBill/${patient.patientid}' />" >Generate Bill</a></td>
</tr>
</c:forEach>
</table> <br><br><br><br>
</div>

</body>
</html>