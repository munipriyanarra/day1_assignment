package com.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dbconnection.DBConnection;
import com.model.UserPet;
import com.dao.UserPetDBDao;


@WebServlet("/AddpetServlet")
public class AddpetServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String age1 = request.getParameter("age");
		String place = request.getParameter("place");
		int age =Integer.parseInt(age1);
		UserPet userpetModel = new UserPet(name, age, place);
	
		try {
			UserPetDBDao regpetUser = new UserPetDBDao(DBConnection.getConnection());
		if (regpetUser.savepetUser(userpetModel)) {
			   response.sendRedirect("petlist.jsp");
			} else {
			
			    response.sendRedirect("profile.jsp");
			    }

		} catch (Exception e) {
	
		e.printStackTrace();
		}
		
		
	}

}