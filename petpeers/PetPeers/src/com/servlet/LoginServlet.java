package com.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dbconnection.DBConnection;
import com.model.User;
import com.dao.UserDBDao;


@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
			 PrintWriter out=response.getWriter();
	       String logname = request.getParameter("name");
	       String logpass = request.getParameter("password");
	       
	       try {
	       	
	       UserDBDao db=  new UserDBDao(DBConnection.getConnection());
	       User user = db.logUser(logname, logpass);
	       
	       if(user!=null){
	           HttpSession session = request.getSession();
	           session.setAttribute("logUser", user);
	           response.sendRedirect("petlist.jsp");
	       }else{
	           out.println("user not found");
	       }
	       }catch(Exception e) {
		
			e.printStackTrace();
		}
		}
	       
	}



