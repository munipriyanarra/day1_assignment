package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.model.UserPet;

public class UserPetDBDao {
	Connection con ;	
	public UserPetDBDao(Connection con) {
        this.con = con;
	}
	
    public boolean savepetUser(UserPet userpet){
        boolean set = false;
        try{
        	PreparedStatement ps=null;
		    ps=this.con.prepareStatement("insert into userpet(name,age,place) values(?,?,?)");
            ps.setString(1, userpet.getName());
           ps.setInt(2, userpet.getAge());
           ps.setString(3, userpet.getPlace());
           ps.executeUpdate();
           set = true;
        }catch(Exception e){
        	System.out.println(e);
        }
        return set;
		
	}
    
}


