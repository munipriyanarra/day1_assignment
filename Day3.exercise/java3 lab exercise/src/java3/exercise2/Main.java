package java3.exercise2;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
			Scanner sc = new Scanner(System.in);
			System.out.println("Enter the string");
			String str=sc.nextLine();
			System.out.println("Enter the character");
			char cha=sc.next().charAt(0);
			System.out.println("The modified string is "+UserMainCode.stringModify(cha,str));
			sc.close();
	}

}
