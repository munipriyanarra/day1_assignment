package Day7exercise;
import java.util.*;

public class DuplicateElements {

	public static void main(String[] args) {
		int n;
		Scanner sin = new Scanner(System.in);
		n = sin.nextInt();
		String[] a1 = new String[n];
		for (int i = 0; i < n; i++) {
		a1[i] = sin.next();
		}
		a1 = orderElements(a1);
		for (int i = 0; i < a1.length; i++) {
		System.out.println("" + a1[i]);
		}
		}
		 
		public static String[] orderElements(String[] arr) {
		HashSet<String> al = new HashSet<String>();
		for (int i = 0; i < arr.length; i++) {
		al.add(arr[i]);
		}
		Iterator<String> itr = al.iterator();
		arr = new String[al.size()];
		int i = 0;
		while (itr.hasNext()) {
		arr[i] = itr.next();
		i++;
		}
		Arrays.sort(arr);
		return arr;

	}

}
