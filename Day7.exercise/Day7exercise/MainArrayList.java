package Day7exercise;

import java.util.Collections;
import java.util.ArrayList;

import java.util.Scanner;

class MainArrayList {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		java.util.ArrayList<Integer> al1=new ArrayList<>();
		ArrayList<Integer> al2=new ArrayList<>();
		System.out.println("enter 5 arraylist1 elements:");
		for(int i=0;i<5;i++) {
			int num=sc.nextInt();
			al1.add(num);
			
		}
		System.out.println("enter 5 arraylist2 elements:");
		for(int i=0;i<5;i++) {
			int num=sc.nextInt();
			al2.add(num);
			
		}
		System.out.println(UserMainCode_ArrayList.sortMergedArrayList(al1,al2));
		
		}

}
