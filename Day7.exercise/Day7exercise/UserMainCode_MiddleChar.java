package Day7exercise;

 class UserMainCode_MiddleChar {
	public static String getMiddleChar(String str) {
		int len=str.length();
		String output=str.substring(len/2-1,len/2+1);
		return output;
	}
}
