package Day7exercise;

public class UserMainCode_IpAddress {
	public static int ipValidator(String str) {
	    String[] array=str.split(".");
	    boolean isTrue=false;
	    for(int i=0;i<array.length;i++) {
	    	int number=Integer.valueOf(array[i]);
	    	if(number>=0&&number<=255)
	    	{
	    		isTrue=true;
	    	}
	    }
	    
	    if(isTrue)
	    	return 1;
	    else
	    	return 2;
	}
}
