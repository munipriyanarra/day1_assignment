package Day7exercise;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class UserMainCode_DateDifference {
	
	public static long getDateDifference(String date1, String date2) {
		LocalDate dt1=LocalDate.parse(date1);
		
		LocalDate dt2=LocalDate.parse(date2);
		long days=ChronoUnit.DAYS.between(dt1,dt2);
		
		return days;
				
		
	}

}
