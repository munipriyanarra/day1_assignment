package Day7exercise;

import java.util.Scanner;

public class Main_MiddleChar {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Enter even length string only :"); 
		String str=input.next();
		String result=UserMainCode_MiddleChar.getMiddleChar(str);
		System.out.println("Middle character in the given string "+result);

	}

}