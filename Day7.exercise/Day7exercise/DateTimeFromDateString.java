package Day7exercise;
import java.util.*;
import java.text.*;

public class DateTimeFromDateString {

	public static void main(String[] args) {
	
		    try {    
		      String originalString = "2021-05-13 18:40:02";
		      Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(originalString);
		      String newstr = new SimpleDateFormat("MM/dd/yyyy, H:mm:ss").format(date);
		       System.out.println(newstr);
		      } 
		    catch (ParseException e) {

		     e.printStackTrace();
		     }
		    
		

	}

}
