package java2.exercise;

 class AboutRoom {
  
  private int roomno;
  private String roomtype;
  private long roomarea;
  private String acmachine;
  
  public int getRoomno() {
	  return roomno;
  }
  public void setRoomno(int roomno) {
	  this.roomno=roomno;
  }
  
  public String getRoomtype() {
	  return roomtype;
  }
  public void setRoomtype(String roomtype) {
	  this.roomtype=roomtype;
  } 
  
  public long getRoomarea() {
	  return roomarea;
  }
  public void setRoomarea(long roomarea) {
	  this.roomarea=roomarea;
  } 
  
  public String getAcmachine() {
	  return acmachine;
  }
  public void setAcmachine(String acmachine) {
	  this.acmachine=acmachine;
  }
  public void displayData()
  {
	  System.out.println("Room Details: ");
	  System.out.println("Room number: " +getRoomno());
	  System.out.println("Room type: " +getRoomtype());
	  System.out.println("Room Area: " +getRoomarea());
	  System.out.println("Ac Machine: " +getAcmachine());
  }
  


}
public class MainRoom {

	public static void main(String[] args) {
		AboutRoom room=new AboutRoom();
		room.setRoomno(100);
		room.setRoomarea(25000);
		room.setRoomtype("flat");
		room.setAcmachine("HP");
		room.displayData();
		

	}

}
