package java2.exercise;
class A{
	public void display() {
		System.out.println("Hey! I am parent class");
	}
}
class B extends A{
	public void diplay() {
		System.out.println("I am Child class");
	}
}

public class ParentChild {

	public static void main(String[] args) {
		A a=new A();
		a.display();
		B b=new B();
		b.display();
		

	}

}
