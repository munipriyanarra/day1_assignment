package java2.exercise;

import java.util.Scanner;

public class SmallestNumber {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("First number: ");
		double x = sc.nextDouble();
		System.out.println("Second number: ");
		double y = sc.nextDouble();
		System.out.println("Third number: ");
		double z = sc.nextDouble();
		System.out.println("The smallest value is " +smallest(x,y,z));

	}
	public static double smallest(double x,double y,double z)
	{
	
		return Math.min(Math.min(x, y), z);
	}

}
