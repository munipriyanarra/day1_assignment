package java2.exercise;

import java.util.Scanner;

public class MidCharSrting {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Input is ");
		String str=sc.nextLine();
		System.out.println("The middle character of a string is " + middle(str));

	}
	
	public static String middle(String str)
	{
		int pos;
		int len;
		if(str.length()%2==0)
		{
			pos=str.length()/2-1;
			len=2;
		}
		else
		{
			pos=str.length()/2;
			len=1;
		}
		return str.substring(pos,pos+len);
	}
}
