package java1.exercise;

import java.util.Scanner;

class UserCode{
	public static int checkSum(int num) {
		 int sum=0;
		 while(num>0) {
			 int a=num%10;
			     if(a%2!=0)
			      {
				 sum=sum+a;
				 num=num/10;
			      }
		 }
			 if(sum%2==0)
				 return -1;
			 else 
				 return 1;
		 
	
	 }
 
public class Main {

	public void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number :");
		int num=sc.nextInt();
		int res=UserCode.checkSum(num);
		if(res == -1)
			System.out.println("Sum of odd digit is even");
		else
			System.out.println("sum of even digit is odd");
		
	 }
   }
 }
