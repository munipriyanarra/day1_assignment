package java1.exercise;

import java.util.Scanner;

public class SwapVariables {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a=sc.nextInt();
		int b=sc.nextInt();
		System.out.println("Before swapping "+a+" "+b);
		int c;
		c=a;
		a=b;
		b=c;
		System.out.println("After swapping " +a+" "+b);

	}

}
