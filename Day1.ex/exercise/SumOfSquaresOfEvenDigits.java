package java1.exercise;
 

import java.util.Scanner;

  class UserMainCode1{
	
	  public static int squares(int a) {
		int sum1=0;
		while(a>0)
		{
			int b=a%10;
			if(b%2==0)
				sum1=sum1+b*b;
			a=a/10;
		}
		return sum1;
	}
}
public class SumOfSquaresOfEvenDigits {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a number ");
		int a=sc.nextInt();
		int result=UserMainCode1.squares(a);
		System.out.println(result);

	}

}
