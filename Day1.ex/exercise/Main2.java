package java1.exercise;

import java.util.Scanner;

class UserMainCode3
{
	public static String getLargestWord(String str) {
		String[] stringarray=str.split(" ");
		int max=0;
		String res="";
		for(int i=0;i<stringarray.length;i++) {
			int len=stringarray[i].length();
			if(max<len) {
				max=len;
				res=stringarray[i];
			}
		}
		return res;
		
	}
}

public class Main2 {

	public void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a string ");
		String str=sc.nextLine();
		System.out.println(UserMainCode3.getLargestWord(str));

	}

}
