package jdbc_exercise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class Mysql_Jdbc {
	public static void main(String[] args) throws Exception{
	    Connection con=null;
	    Statement st=null;
	    try
	    {
	       
	     Class.forName("com.mysql.cj.jdbc.Driver");//
	    //DriverManager.registerDriver(new com.mysql.jdbc.Driver());
	    con=DriverManager.getConnection("jdbc:mysql://localhost:3306/sample","root","root");
	    st=con.createStatement();
	   st.execute("create table users(username varchar(30),password varchar(30),fullname varchar(30),email varchar(30))");
	    int noOfRows=st.executeUpdate("insert into users values('steve','secretpass','steve paul','steve.pul@hcl.com')");
		System.out.println(noOfRows+" row inserted");
		    }
	    catch(Exception e)
	    {
	        System.out.println(e);
	    }
	    finally
	    {
	        st.close();
	        con.close();
	    }
	
	}
}
