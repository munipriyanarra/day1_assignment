package jdbc_exercise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Mysql_jdbc1 {
	 public static void main(String[] args) throws Exception {
	        Connection con = null;
	        Statement st = null;
	        ResultSet rs = null;
	        try {
	             Class.forName("com.mysql.cj.jdbc.Driver");//
	            // DriverManager.registerDriver(new com.mysql.jdbc.Driver());
	            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/sample?useSSL=false&serverTimezone=UTC", "root", "root");
	            st = con.createStatement();
	            rs = st.executeQuery("select * from users");
	            while (rs.next()) {
	                //System.out.println(rs.getInt("id") + " " + rs.getString("name"));
	                System.out.println(rs.getString("username")+" "+rs.getString("password")+" "+rs.getString("fullname")+" "+rs.getString("email"));
	            }
	            con.close();
	        }
	       
	        catch (Exception e) {
	            System.out.println(e);
	        } 
	        
	    }
	 
	 
	}


