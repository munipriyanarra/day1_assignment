package jdbc_exercise;

import java.sql.Connection;
import java.sql.Statement;

public class Mysql_jdbc3 {
	public static void main (String[] args) throws Exception{
		Connection con=null;
		Statement st=null;
		try {
			con=DBConnection.getConnection();
			st=con.createStatement();
			String sql="delete from users where username='steve'";
			
			int a=st.executeUpdate(sql);
			System.out.println(a+ "rows deleted");
		} catch (Exception e) {
            System.out.println(e);
        }
	}

}
