package java4.exercise;

public class Rectangle extends Shape {
	private Integer length;
	private Integer width;
	public Integer getLength() {
		return length;
	}
	
	public void setLength(Integer length) {
		this.length=length;
	}
	public Integer getWidth() {
		return width;
	}
	
	public void setWidth(Integer width) {
		this.width=width;
	}
	public Float calculateArea() {
		Float area=Float.valueOf((float)length*width);
				return area;
	}
public Rectangle(String name,Integer length,Integer width) {
	super(name);
	this.length=length;
	this.width=width;
}
	

}
