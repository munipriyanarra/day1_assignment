package java4.exercise;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Shape name");
		System.out.println("1.Circle\n2.Rectangle\n3.Square");
		String choice=sc.nextLine();
			if(choice.equals("Circle")) {
				System.out.println("Enter the radius");
				int radius=sc.nextInt();
				Circle cir=new Circle("cir",radius);
				System.out.println("%.2f",cir.calculateArea());
			}
			else if(choice.equals("Circle")) {
			System.out.println("Enter the length");
			int length=sc.nextInt();
			System.out.println("Enter the width");
			int width=sc.nextInt();
			Rectangle rec=new Rectangle("Rec",length,width);
			System.out.println("%.2f",rec.calculateArea());
		}
			else if(choice.equals("Square")) {
			System.out.println("Enter the side");
			int side=sc.nextInt();
			Square sq=new Square("sq",side);
			System.out.println("%.2f",sq.calculateArea());
		}
				sc.close();

	}

}
