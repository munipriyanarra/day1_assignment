
package java4.exercise2;

public class MyCalculator implements AdvancedArithmetic {

	      public int divisor_sum(int n) {
			 int sum=0;
			 for(int i=1;i<=n;++i) {
				 if(n%i==0)
				 {
					 sum=sum+i;
				 }
			 }
			 return sum;
		}

	}
	public interface AdvancedArithmetic {
		
		public default void myInterface() {
			System.out.println("I implemented :AdvancedArithmetic");
		}
		public abstract int divisor_sum(int n);
	}


