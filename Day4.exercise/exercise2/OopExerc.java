package java4.exercise2;

class B extends S{
	int b=50;
	public void display() {
		System.out.println("b value "+b);
	}
}
class C extends S{
	int c=500;
	public void display() {
		System.out.println("c value "+c);
	}
}

public class OopExerc {
	 static int a = 555; 
	 public static void main(String[] args) { 
		 S objA = new S(); 
		 B objB1 = new B();
		 S objB2 = new B();
		 C objC1 = new C();
		 B objC2 = new C(); 
		 S objC3 = new C(); 
		 objA.display(); 
		 objB1.display(); 
		 objB2.display(); 
		 objC1.display();
		 objC2.display(); 
		 objC3.display();
		 }
	 } 
		class S {
		int a = 100;
		public void display() {
		System.out.printf("a in A = %d\n", a);
		}
		}



